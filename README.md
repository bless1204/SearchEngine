# **SearchEngine**

## Dependencies
- [Node] (https://nodejs.org/en/download/)

## Installation Steps
- Install Dependencies
- Go to project folder and install other dependencides via npm

```
cd SearchEngine
npm install
```

- (Optional) Install sails globally

```
npm install -g sails
```

- (Optional) Install forever globally

```
npm install -g forever
```

- Change database properties in config/connections.js to your own settings. Make sure to change the three connections.
- Run project. Can be run in three ways:
  
  ```
  node app.js
  ```

  If sails is installed globally:
  ```
  sails lift
  ```

  If not
  ```
  ./node_modules/sails lift
  ```

  OR run through forever
  ```
  forever start app.js
  ```

** Make sure that port 1337 (sails run in port 1337 by default) is free, otherwise uncomment port in config/local.js and set it to desired port