module.exports = {
  connection: 'enhancedDB',
  tableName: 'CRAWLED_URLS',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  autoPK: false,
  attributes: {
    URL: {
      type: 'String'
    },
    TITLE: {
      type: 'String'
    },
    BODY: {
      type: 'String'
    },
    KEYWORD: {
      type: 'String'
    }
  }
}
