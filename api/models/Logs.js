module.exports = {
  connection: 'appDB',
  tableName: 'LOGS',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  autoPK: false,
  attributes: {
    SEARCH_ID: {
      type: 'Integer',
      primaryKey: true,
      autoIncrement: true
    },
    SEARCH_WORD: {
      type: 'String'
    },
    ALGORITHM: {
      type: 'String'
    },
    START_TIME: {
      type: 'Integer'
    },
    END_TIME: {
      type: 'Integer'
    }
  }
}
