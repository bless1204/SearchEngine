/**
 * ExistingController
 *
 * @description :: Server-side logic for managing Existings
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var Fuse = require('fuse.js');

module.exports = {
  
  getSearchUrls: function (req, res) {
    var searchWord = req.params.searchWord;
    var startTime = Date.now();
    var endTime;

    async.auto({
      fetchFromDb: function(asyncCb) {
        var options = {
          or: [
            { TITLE: { 'like': '%'+searchWord+'%'} },
            { KEYWORD: { 'like': '%'+searchWord+'%'} },
            { BODY: { 'like': '%'+searchWord+'%'} }
          ]
        };
        Enhanced.find(options, function(err, data){
          asyncCb(null, data);
        });
      },
      buildReturnData: ['fetchFromDb', function(asyncCb, result){
        var results = [];
        result.fetchFromDb.forEach(function(obj){
          var data = {
            Title: obj.TITLE,
            Url: obj.URL
          };
          results.push(data);
        }); 
        var returnData = {
          count: results.length,
          results: results
        };
        asyncCb(null, returnData);
      }],
      saveToLogs: ['buildReturnData', function(asyncCb, result){
        endTime = Date.now();
        var logs = {
          SEARCH_WORD: searchWord,
          ALGORITHM: 'Existing',
          START_TIME: startTime,
          END_TIME: endTime
        };
        sails.log.info('Saving to Logs table: ');
        sails.log.info(logs);
        Logs
          .create(logs, function(err, data){
            if(err) {
              sails.log.error(err);
            }
            asyncCb(err, data);
          });
      }]
    }, function(error, results){
      if(error){
        return res.json('An error occurred: ' + JSON.parse(error));
      }
      return res.json(results.buildReturnData);
    });
  }

};