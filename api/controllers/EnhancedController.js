/**
 * EnhancedController
 *
 * @description :: Server-side logic for managing Enhanceds
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var Fuse = require('fuse.js');

module.exports = {
  
  getSearchUrls: function (req, res) {
    var searchWord = req.params.searchWord;
    var startTime = Date.now();
    var endTime;

    async.auto({
      fetchFromDb: function(asyncCb) {
        var options = {
          or: [
            { KEYWORD: { 'like': '%'+searchWord+'%'} },
            { BODY: { 'like': '%'+searchWord+'%'} }
          ]
        };
        Enhanced.find(options, function(err, data){
          asyncCb(null, data);
        });
      },
      doFuzzyQuery: ['fetchFromDb', function(asyncCb, result){
        var options = {
          caseSensitive: false,
          shouldSort: true,
          threshold: 0.8,
          include: ['score'],
          maxPatternLength: searchWord.length,
          keys: ['KEYWORD', 'BODY']
        };
        var fuse = new Fuse(result.fetchFromDb, options);
        var result = fuse.search(searchWord);
        endTime = Date.now();
        asyncCb(null, result);
      }],
      buildReturnData: ['doFuzzyQuery', function(asyncCb, result){
        var results = [];
        result.doFuzzyQuery.forEach(function(obj){
          var data = {
            Title: obj.item.TITLE,
            Url: obj.item.URL,
            Score: obj.score
          };
          results.push(data);
        }); 
        var returnData = {
          count: results.length,
          results: results
        };
        asyncCb(null, returnData);
      }],
      saveToLogs: ['doFuzzyQuery', function(asyncCb, result){
        var logs = {
          SEARCH_WORD: searchWord,
          ALGORITHM: 'Enhanced',
          START_TIME: startTime,
          END_TIME: endTime
        };
        sails.log.info('Saving to Logs table: ');
        sails.log.info(logs);
        Logs
          .create(logs, function(err, data){
            if(err) {
              sails.log.error(err);
            }
            asyncCb(err, data);
          });
      }]
    }, function(error, results){
      if(error){
        return res.json('An error occurred: ' + JSON.parse(error));
      }
      return res.json(results.buildReturnData);
    });
  }

};


